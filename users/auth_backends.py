# coding=utf-8
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from .models import User


class EmailAuthBackend(object):
    # TODO: Enable users to login with email
    def authenticate(self, username=None, password=None):
        try:
            validate_email(username)
        except ValidationError:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                return None
        else:
            try:
                user = User.objects.get(email=username)
            except User.DoesNotExist:
                return None

        if user.check_password(password):
            return user
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
